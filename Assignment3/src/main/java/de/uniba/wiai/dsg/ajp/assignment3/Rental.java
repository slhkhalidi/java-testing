package de.uniba.wiai.dsg.ajp.assignment3;

public class Rental {

	private int daysRented;
	private Movie movie;
	private double discount;


	public Movie getMovie() {return movie; }

	/**
	 * Set the movie for rental
	 *
	 * Precondition:
	 * <ul>
	 *     <li>The Movie must not be null or empty</li>
	 * </ul>
	 *
	 * Postcondition
	 * <ul>
	 *     <li>The Movie given in parameter will be set to be the movie of rental at the end.</li>
	 * </ul>
	 *
	 * @param movie The Movie for rental. If the Movie is null or empty nothing will be created.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
	 */
	public void setMovie(Movie movie) throws IllegalArgumentException {
		if(movie==null) {
			throw new IllegalArgumentException("ERROR IN MOVIE");
		}
		this.movie = movie;
	}

	public int getDaysRented() {
		return daysRented;
	}

	/**
	 * Set the numbers of days for rental
	 *
	 * Precondition:
	 * <ul>
	 *     <li>The days must bigger than 0 </li>
	 * </ul>
	 *
	 * Postcondition
	 * <ul>
	 *     <li>The day given in parameter will be set to be the length of rental period at the end.</li>
	 * </ul>
	 *
	 * @param daysRented The numbers of days for rental. If the day is smaller than 0 nothing will be created.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
	 */
	public void setDaysRented(int daysRented) throws IllegalArgumentException {
		if(daysRented <= 0) {
			throw new IllegalArgumentException("ERROR IN DAYS RENTED");
		}
		this.daysRented = daysRented;
	}

	/**
	 * Gets the Price's Charge.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>return the price's Charge in the given days rented, movie's resolution and discount as a double</li>
	 * </ul>
	 *
	 * @return A double representing Price's charge.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
	 */
	public double getCharge() {
		if (movie.getResolution().equals("4K")) {
			return ((movie.getCharge(daysRented) + 2) * (1 - this.discount));
		}
		else if(movie.getResolution().equals("HD")) {
			return (movie.getCharge(daysRented) * (1 - this.discount));
		}else {
			throw new IllegalArgumentException("ERROR IN RESOLUTION");
		}
	}

	/**
	 * Gets the Frequent Renter Points.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>return the Frequent Renter Points</li>
	 * </ul>
	 *
	 * @return Integer representing Frequent Renter Points .
	 *
	 */
	public int getFrequentRenterPoints() {
		return movie.getFrequentRenterPoints(daysRented);
	}

	/**
	 * Sets the Discount.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>Discount must between 0 and 1.</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>set discount for the movie.</li>
	 * </ul>
	 *
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
	 */
	public void setDiscount(double discount) {
		if (discount < 0.0 || discount > 1.0) {
			throw new IllegalArgumentException("Invalid discount value");
		}
		this.discount = discount;
	}

	public double getDiscount(){
		return this.discount;
	}


}
