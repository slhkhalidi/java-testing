package de.uniba.wiai.dsg.ajp.assignment3;

public enum PriceCategory {
    REGULAR(0),
    NEW_RELEASE(1),
    CHILDRENS(2),
    LOW_BUDGET(3);
    private final int priceCode;

    PriceCategory(final int priceCode){
        this.priceCode = priceCode;
    }

    public int getValue() {
        return priceCode;
    }



}
