package de.uniba.wiai.dsg.ajp.assignment3;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents an Customer
 *
 * @author Salah Khalidi
 * @version 1.0
 * @since 17.06.2021
 */
public class Customer {

	private String name;
	private List<Rental> rentals = new LinkedList<Rental>();

	/**
	 * Creates an Customer with the specified name.
	 *
	 * Precondition:
	 * <ul>
	 *    <li>The name must not be null or empty</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>Creating a new Customer with the given String name in the parameters</li>
	 * </ul>
	 *
	 * @param name The Customer’s name. If the String name is Empty or null nothing will be created.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
     */
	public Customer(String name) throws IllegalArgumentException {
		super();
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("ERROR IN NAME");
		} else {
			this.name = name;
		}
	}

	public String getName() {
		return name;
	}


	/**
	 * Sets the Customer’s name.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>The name must not be null or empty</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>set the given String name in the customer's name</li>
	 * </ul>
	 *
	 * @param name The Customer’s name. If the String name is Empty or null nothing will be seated.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
     */
	public void setName(String name) throws IllegalArgumentException {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("ERROR IN NAME");
		} else {
			this.name = name;
		}
	}

	public List<Rental> getRentals() {
		return rentals;
	}

	/**
	 * Sets the List of the Customer’s rentals.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>The list must not be empty or null</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>set the given rental's list in the customer's rentals</li>
	 * </ul>
	 *
	 * @param rentals The list of Rentals. If the List is Empty nothing will be seated.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met.
     */
	public void setRentals(List<Rental> rentals) throws IllegalArgumentException {
		if (rentals.isEmpty() || rentals==null) {
			throw new IllegalArgumentException("ERROR IN RENTALS LIST");
		} else {
			this.rentals = rentals;
		}
	}


	/**
	 * Create an invoice in Text Form
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>create a String invoice including the Customer's name,
	 *         movie's title, the TotalCharge owed and the earned frequent renter points</li>
	 * </ul>
	 *
	 * @return String representing the invoice in String Form
	 * @throws IllegalArgumentException if there are errors while the invoice's creation.
     */
	public String statement() throws IllegalArgumentException {
		String result = "Rental Record for " + getName() + "\n";

		int frequentRenterPoints = 0;
		for (Rental each : this.rentals) {
			frequentRenterPoints += each.getFrequentRenterPoints();

			// show figures for this rental
			result += "\t" + each.getMovie().getTitle() + "\t" + each.getMovie().getResolution() + "\t"
					+ String.valueOf(each.getCharge()) + "\n";
		}

		// add footer lines
		result += "Amount owed is " + String.valueOf(getTotalCharge()) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints)
				+ " frequent renter points";
		return result;
	}

	/**
	 * Create an invoice in HTML Form
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Post condition:
	 * <ul>
	 *     <li>create an invoice String in HTML form including the Customer's name,
	 *         movie's title, the TotalCharge owed and the earned frequent renter points</li>
	 * </ul>
	 *
	 * @return String representing the invoice in HTML form
	 * @throws IllegalArgumentException if there are errors while the invoice's creation.
	 */
	public String htmlStatement() throws IllegalArgumentException {
		String result = "<H1>Rentals for <EM>" + getName() + "</EM></H1><P>\n";

		for (Rental each : rentals) {
			// show figures for each rental
			result += each.getMovie().getTitle() + " " + each.getMovie().getResolution() + ": "
					+ String.valueOf(each.getCharge()) + "<BR>\n";
		}

		// add footer lines
		result += "<P>You owe <EM>" + String.valueOf(getTotalCharge())
				+ "</EM><P>\n";
		result += "On this rental you earned <EM>"
				+ String.valueOf(getTotalFrequentRenterPoints())
				+ "</EM> frequent renter points<P>";
		return result;
	}


	/**
	 * Get the total of the charge
	 *
	 * Precondition
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Post condition:
	 * <ul>
	 *     <li>calculate the Total of the Charge in the rentalsList and return it as a double</li>
	 * </ul>
	 *
	 * @return double representing the Total of the Charge
	 * @throws IllegalArgumentException if there are errors while the charge addition
     */
	double getTotalCharge() throws IllegalArgumentException {
		double result = 0;
		if (rentals.isEmpty() || rentals==null) {
			throw new IllegalArgumentException("ERROR IN RENTALS LIST");
		}
		for (Rental each : rentals) {
			result += each.getCharge();
		}

		return result;
	}



	/**
	 * Get the total of the frequent renter points
	 *
	 * Precondition
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Post condition:
	 * <ul>
	 *     <li>calculate the total of the frequent renter points in the rentals list and return it as a Integer</li>
	 * </ul>
	 *
	 * @return integer representing the Total of the frequent renter points
	 * @throws IllegalArgumentException if there are errors while the frequent renter points addition
	 */
	int getTotalFrequentRenterPoints() throws IllegalArgumentException {
		int result = 0;
		if (rentals.isEmpty() || rentals==null) {
			throw new IllegalArgumentException("ERROR IN RENTALS LIST");
		}
		for (Rental each : rentals) {
			result += each.getFrequentRenterPoints();
		}

		return result;
	}

}

