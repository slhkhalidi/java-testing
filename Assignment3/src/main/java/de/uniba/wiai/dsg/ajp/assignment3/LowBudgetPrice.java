package de.uniba.wiai.dsg.ajp.assignment3;

public class LowBudgetPrice extends Price{
    @Override
    double getCharge(int daysRented) {
        double result;
        result = daysRented*0.5;
        return result;
    }

    @Override
    int getPriceCode() {
        return PriceCategory.LOW_BUDGET.getValue();
    }
}
