package de.uniba.wiai.dsg.ajp.assignment3;

/**
 * Represents an Movie
 *
 * @author Group.18
 * @version 1.0
 * @since 17.06.2021
 */
public class Movie {

	private Price price;
	private String title;
	private String resolution;

	/**
	 * Creates an new Movie with specified title, price and resolution.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Postcondition
	 * <ul>
	 *     <li>The Title given in parameter will be set to the movie's title with resolution at the end.</li>
	 *     <li>Search the Price dependent on the price code given in parameter and it will be set to the movie's price
	 *         by the method setPriceCode</li>
	 *     <li>The resolution given in parameter will be set to the movie's resolution</li>
	 * </ul>
	 *
	 * @param title The Movie's title. If the Title is null or empty nothing will be created.
	 * @param priceCode The Movie's PriceCode. If the PriceCode is negative  or more than 3 nothing will be created.
	 * @param resolution The Movie's resolution. If the Resolution is not 4K or HD, nothing will be created.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
     */
	public Movie(String title, int priceCode, String resolution) {
		this.setTitle(title);
		this.setPriceCode(priceCode);
		this.setResolution(resolution);
	}

	public String getTitle() {
		return title;
	}

	/**
	 * Set title from the Movie.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>The Title must not be null or empty</li>
	 * </ul>
	 *
	 * Postcondition
	 * <ul>
	 *     <li>The Title given in parameter will be set to the movie's title with resolution at the end.</li>
	 * </ul>
	 *
	 * @param title The Movie's title. If the Title is null or empty nothing will be created.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
	 */
	public void setTitle(String title) throws IllegalArgumentException {
		if(title == null || title.isEmpty()){
			throw new IllegalArgumentException("ERROR IN TITLE");
		}
		this.title = title;
	}

	String getResolution(){
		return this.resolution;
	}

	/**
	 * Set movie's resolution.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>The Resolution must be 4K or HD</li>
	 * </ul>
	 *
	 * Postcondition
	 * <ul>
	 *     <li>The resolution given in parameter will be set to the movie's resolution</li>
	 * </ul>
	 * @param resolution The Movie's resolution. If the Resolution is not 4K or HD, nothing will be created.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
	 */
	public void setResolution(String resolution) throws IllegalArgumentException {
		if(resolution.equals("HD") || resolution.equals("4K")) {
			this.resolution = resolution;
		}else{
			throw new IllegalArgumentException("INVALID RESOLUTION.");
		}
	}


	/**
	 * Gets the Price's Charge.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>return the price's Charge in the given days rented as a double</li>
	 * </ul>
	 *
	 * @param daysRented numbers of days for rental movie.
	 * @return A double representing Price's charge in the daysRented.
	 * @throws IllegalArgumentException if any of the above preconditions are not
	 *                                   met .
     */
	double getCharge(int daysRented) {
		return price.getCharge(daysRented);
	}


	/**
	 * Gets the Price's Code.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>return the price's code as an Integer</li>
	 * </ul>
	 *
	 *
	 * @return Integer representing the Price's code .
	 */
	public int getPriceCode() {
		return price.getPriceCode();
	}


    /**
	 * Sets the Movie's Price dependent on the price code, and create a new category for that price code.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>The given priceCode should be exists</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>Set the movie's price dependent on the given price code</li>
	 * </ul>
	 *
	 * @param priceCode If the PriceCode are not exists it will be nothing seated.
	 *@throws IllegalArgumentException if the price code are incorrect.
     */
	public void setPriceCode(int priceCode) {
		if(priceCode == PriceCategory.REGULAR.getValue()) {
			price = new RegularPrice();
		}
		else if(priceCode == PriceCategory.CHILDRENS.getValue()) {
			price = new ChildrensPrice();
		}
		else if(priceCode == PriceCategory.NEW_RELEASE.getValue()) {
			price = new NewReleasePrice();
		}
		else if(priceCode == PriceCategory.LOW_BUDGET.getValue()) {
			price = new LowBudgetPrice();
		}else{
			throw new IllegalArgumentException("Incorrect Price Code");
		}
	}





	/**
	 * Gets the Frequent Renter Points.
	 *
	 * Precondition:
	 * <ul>
	 *     <li>none</li>
	 * </ul>
	 *
	 * Postcondition:
	 * <ul>
	 *     <li>return the Frequent Renter Points in the given days rented</li>
	 * </ul>
	 *
	 * @param daysRented
	 * @return Integer representing Frequent Renter Points .
	 *  @throws IllegalArgumentException if any error is  hit.
	 *
	 */
	public int getFrequentRenterPoints(int daysRented) {
			return price.getFrequentRenterPoints(daysRented);
	}

}
