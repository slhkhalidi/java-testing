package de.uniba.wiai.dsg.ajp.assignment3;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class IntegrationTests {

    @Test
    void integrationTestStatement() {
            // given
            Movie movietest = new Movie("Test",2, "HD");
            Customer customertest = new Customer("x");
            Rental rentaltest = new Rental();
            rentaltest.setDaysRented(4);
            rentaltest.setMovie(movietest);
            List<Rental> rentalstest = new LinkedList<Rental>();
            rentalstest.add(rentaltest);
            customertest.setRentals(rentalstest);

            // when
            String testing = customertest.statement();

            // then
            assertEquals("Rental Record for x\n\tTest\tHD\t3.0\nAmount owed is 3.0\nYou earned 1 frequent renter points",testing);
        }



    @Test
    void integrationTestHtmlStatement() {
        // given
        Movie movietest = new Movie("Test",2, "HD");
        Customer customertest = new Customer("x");
        Rental rentaltest = new Rental();
        rentaltest.setDaysRented(4);
        rentaltest.setMovie(movietest);
        List<Rental> rentalstest = new LinkedList<Rental>();
        rentalstest.add(rentaltest);
        customertest.setRentals(rentalstest);

        // when
        String testing = customertest.htmlStatement();

        // then
        assertEquals("<H1>Rentals for <EM>x</EM></H1><P>\nTest HD: 3.0<BR>\n<P>You owe <EM>3.0</EM><P>\nOn this rental you earned <EM>1</EM> frequent renter points<P>",testing);
    }
}