package de.uniba.wiai.dsg.ajp.assignment3;

import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;

public class UnitTests {


    // CHILDRENSPRICE TEST

    @Test
    public void stubGetChargeChildrenAbove3Correctly() {
        // given
        ChildrensPrice stubbedCPrice = mock(ChildrensPrice.class, CALLS_REAL_METHODS);

        // when
        double result = stubbedCPrice.getCharge(7);

        // then
        assertEquals(7.5, result, "The method does not charge the correct value");
    }

    // CHILDRENSPRICE TEST END




    // REGULARPRICE TEST

    @Test
    public void stubGetChargeRegularAbove2Correctly() {
        // given
        RegularPrice stubbedRPrice = mock(RegularPrice.class, CALLS_REAL_METHODS);

        // when
        double result = stubbedRPrice.getCharge(7);

        // then
        assertEquals(9.5, result, "The method does not charge the correct value");
    }

    // REGULARPRICE TEST END




    // NEWRELEASEPRICE TESTS

    @Test
    public void stubGetChargeNewReleaseCorrectly() {
        // given
        NewReleasePrice stubbedNEWPrice = mock(NewReleasePrice.class, CALLS_REAL_METHODS);

        // when
        double result = stubbedNEWPrice.getCharge(4);

        // then
        assertEquals(12, result, "The method does not charge the correct value");
    }

    @Test
    public void stubGet2FrequentRenterPointsCorrectly() {
        // given
        NewReleasePrice stubbedNEWPrice = mock(NewReleasePrice.class, CALLS_REAL_METHODS);

        // when
        int result = stubbedNEWPrice.getFrequentRenterPoints(4);

        // then
        assertEquals(2, result, "The method does not calculate the correct amount of Frequent Renter Points");
    }

    @Test
    public void stubGet1FrequentRenterPointCorrectly() {
        // given
        NewReleasePrice stubbedNEWPrice = mock(NewReleasePrice.class, CALLS_REAL_METHODS);

        // when
        int result = stubbedNEWPrice.getFrequentRenterPoints(1);

        // then
        assertEquals(1, result, "The method does not calculate the correct amount of Frequent Renter Points");
    }

    // NEWRELEASEPRICE TESTS END




    // LOWBUDGETPRICE TEST

    @Test
    public void stubGetChargeLowBudgetCorrectly() {
        // given
        LowBudgetPrice stubbedLOWPrice = mock(LowBudgetPrice.class, CALLS_REAL_METHODS);

        // when
        double result = stubbedLOWPrice.getCharge(4);

        // then
        assertEquals(2, result, "The method does not charge the correct value");
    }

    // LOWBUDGETPRICE TEST END




    // PRICE TEST

    @Test
    public void stubGetCorrectAmountOfDefaultFrequentRenterPoints() {
        // given
        Price stubbedPrice = mock(Price.class, CALLS_REAL_METHODS);

        // when
        int result = stubbedPrice.getFrequentRenterPoints(5);

        // then
        assertEquals(1, result, "The method does not return the correct default amount of Frequent Renter Points");
    }

    // PRICE TEST END




    // RENTAL TESTS

    @Test
    public void stubThrowExceptionSetMovie() {
        // given
        Rental stubbedRental = mock(Rental.class);
        Movie stubbedMovie = mock(Movie.class);
        willThrow(new IllegalArgumentException()).given(stubbedRental).setMovie(stubbedMovie);

        // when
        Executable invalidInput = () -> {stubbedRental.setMovie(stubbedMovie);};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Rental");
    }

    @Test
    public void stubThrowExceptionSetDaysRented() {
        // given
        Rental stubbedRental = mock(Rental.class);
        willThrow(new IllegalArgumentException()).given(stubbedRental).setDaysRented(anyInt());

        // when
        Executable invalidInput = () -> {stubbedRental.setDaysRented(0);};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Rental");
    }

    @Test
    public void stubThrowExceptionSetDiscount() {
        // given
        Rental stubbedRental = mock(Rental.class);
        willThrow(new IllegalArgumentException()).given(stubbedRental).setDiscount(anyDouble());

        // when
        Executable invalidInput = () -> {stubbedRental.setDiscount(2.5);};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Rental");
    }

    @Test
    public void stubGetCharge4KResolutionCorrectly() {
        // given
        Rental stubbedRental = mock(Rental.class, CALLS_REAL_METHODS);
        Movie stubbedMovie = mock(Movie.class);
        stubbedRental.setMovie(stubbedMovie);
        stubbedRental.setDiscount(0.2);
        given(stubbedMovie.getResolution()).willReturn("4K");
        given(stubbedMovie.getCharge(anyInt())).willReturn(3.5);

        // when
        double result = stubbedRental.getCharge();

        // then
        assertEquals(4.4, result, "The method does not charge the correct value");
    }

    // RENTAL TESTS END




    // MOVIE TESTS

    @Test
    public void mockSetPriceCodeValidInputCorrectly() {   // Would be the exact same test for all other valid inputs
        // given
        Movie stubbedMovie = mock(Movie.class, CALLS_REAL_METHODS);

        // when
        stubbedMovie.setPriceCode(1);

        // then
        assertEquals(1, stubbedMovie.getPriceCode(), "The method does not apply the correct PriceCode");
    }

    @Test
    public void stubThrowExceptionSetPriceCode() {
        // given
        Movie stubbedMovie = mock(Movie.class);
        willThrow(new IllegalArgumentException()).given(stubbedMovie).setPriceCode(anyInt());

        // when
        Executable invalidInput = () -> {stubbedMovie.setPriceCode(5);};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Movie");
    }

    @Test
    public void stubThrowExceptionSetTitle() {
        // given
        Movie mockedMovie = mock(Movie.class);
        willThrow(new IllegalArgumentException()).given(mockedMovie).setTitle(anyString());

        // when
        Executable invalidInput = () -> {mockedMovie.setTitle("nothing");};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Movie");
    }

    @Test
    public void stubThrowExceptionSetResolution() {
        // given
        Movie mockedMovie = mock(Movie.class);
        willThrow(new IllegalArgumentException()).given(mockedMovie).setResolution(anyString());

        // when
        Executable invalidInput = () -> {mockedMovie.setResolution("null");};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Movie");
    }

    // MOVIE TESTS END




    // CUSTOMER TESTS

    @Test
    public void stubCustomerConstructorException() {
        // given
        String name = null;

        // when
        Executable invalidInput = () -> {Customer testCustomer = new Customer(name);};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Customer");
    }

    @Test
    public void stubSetNameException() {
        // given
        Customer stubbedCustomer = mock(Customer.class);
        willThrow(new IllegalArgumentException()).given(stubbedCustomer).setName(anyString());

        // when
        Executable invalidInput = () -> {stubbedCustomer.setName("invalid");};

        // then
        assertThrows(IllegalArgumentException.class, invalidInput, "Expected the IllegalArgumentException from Customer");
    }

    @Test
    public void stubGetTotalChargeException() {
        // given
        Customer stubbedCustomer = mock(Customer.class);
        willThrow(new IllegalArgumentException()).given(stubbedCustomer).getTotalCharge();

        // when
        Executable invalidRentals = () -> {stubbedCustomer.getTotalCharge();};

        // then
        assertThrows(IllegalArgumentException.class, invalidRentals, "Expected the IllegalArgumentException from Customer");
    }

    //Mock + Stub//
    @Test
    public void mockGetTotalChargeCorrectly() {
        // given
        Customer mockedCustomer = mock(Customer.class, CALLS_REAL_METHODS);
        Rental mockedRental1 = mock(Rental.class);
        Rental mockedRental2 = mock(Rental.class);
        mockedRental1.setDaysRented(3);
        mockedRental2.setDaysRented(3);
        mockedRental1.setDiscount(0.4);
        mockedRental2.setDiscount(0.2);
        List<Rental> testRentals = new LinkedList<>();
        testRentals.add(mockedRental1);
        testRentals.add(mockedRental2);
        mockedCustomer.setRentals(testRentals);
        given(mockedRental1.getCharge()).willReturn(1.5);
        given(mockedRental2.getCharge()).willReturn(2.5);

        // when
        double result = mockedCustomer.getTotalCharge();

        // then
        assertEquals(4, result, "The method does not charge the correct total amount");
        then(mockedRental1).should(times(1)).getCharge();
        then(mockedRental2).should(times(1)).getCharge();
    }

    @Test
    public void stubGetTotalFrequentRenterPointsException() {
        // given
        Customer stubbedCustomer = mock(Customer.class);
        willThrow(new IllegalArgumentException()).given(stubbedCustomer).getTotalFrequentRenterPoints();

        // when
        Executable invalidRentals = () -> {stubbedCustomer.getTotalFrequentRenterPoints();};

        // then
        assertThrows(IllegalArgumentException.class, invalidRentals, "Expected the IllegalArgumentException from Customer");
    }

    @Test
    public void mockGetTotalFrequentRenterPointsCorrectly() {
        // given
        Customer mockedCustomer = mock(Customer.class, CALLS_REAL_METHODS);
        Rental mockedRental1 = mock(Rental.class, CALLS_REAL_METHODS);
        Rental mockedRental2 = mock(Rental.class, CALLS_REAL_METHODS);
        Movie mockedMovie1 = mock(Movie.class);
        Movie mockedMovie2 = mock(Movie.class);
        mockedRental1.setMovie(mockedMovie1);
        mockedRental2.setMovie(mockedMovie2);
        mockedRental1.setDaysRented(3);
        mockedRental2.setDaysRented(3);
        List<Rental> testRentals = new LinkedList<>();
        testRentals.add(mockedRental1);
        testRentals.add(mockedRental2);
        mockedCustomer.setRentals(testRentals);
        given(mockedMovie1.getFrequentRenterPoints(3)).willReturn(1);
        given(mockedMovie2.getFrequentRenterPoints(3)).willReturn(2);

        // when
        int result = mockedCustomer.getTotalFrequentRenterPoints();

        // then
        assertEquals(3, result, "The method does not calculate the correct amount of total FrequentRenterPoints");
        then(mockedRental1).should(times(1)).getFrequentRenterPoints();
        then(mockedRental2).should(times(1)).getFrequentRenterPoints();
    }

    @Test
    public void stubSetRentalsException() {
        // given
        Customer stubbedCustomer = mock(Customer.class);
        Rental mockedRental = mock(Rental.class);
        List<Rental> testRentals = new LinkedList<>();
        testRentals.add(mockedRental);
        willThrow(new IllegalArgumentException()).given(stubbedCustomer).setRentals(testRentals);

        // when
        Executable invalidRentals = () -> {stubbedCustomer.setRentals(testRentals);};

        // then
        assertThrows(IllegalArgumentException.class, invalidRentals, "Expected the IllegalArgumentException from Customer");
    }

    @Test
    public void mockStatementRepeatForLoopCorrectly() {
        // given
        Movie mockedMovie1 = mock(Movie.class, CALLS_REAL_METHODS);
        Movie mockedMovie2 = mock(Movie.class, CALLS_REAL_METHODS);
        Rental mockedRental1 = mock(Rental.class, CALLS_REAL_METHODS);
        Rental mockedRental2 = mock(Rental.class, CALLS_REAL_METHODS);
        Customer mockedCustomer = mock(Customer.class, CALLS_REAL_METHODS);
        mockedMovie1.setPriceCode(3);
        mockedMovie1.setTitle("Hardcore Testing");
        mockedMovie1.setResolution("HD");
        mockedMovie2.setPriceCode(3);
        mockedMovie2.setTitle("Softcore Testing");
        mockedMovie2.setResolution("HD");
        mockedRental1.setMovie(mockedMovie1);
        mockedRental1.setDaysRented(1);
        mockedRental1.setDiscount(0.9);
        mockedRental2.setMovie(mockedMovie2);
        mockedRental2.setDaysRented(1);
        mockedRental2.setDiscount(0.9);
        List<Rental> testRentals = new LinkedList<>();
        testRentals.add(mockedRental1);
        testRentals.add(mockedRental2);
        mockedCustomer.setName("Werner");
        mockedCustomer.setRentals(testRentals);

        // when
        mockedCustomer.statement();

        // then
        then(mockedRental1).should(times(1)).getFrequentRenterPoints();
        then(mockedRental2).should(times(1)).getFrequentRenterPoints();
    }

    @Test
    public void mockHTMLStatementRepeatForLoopCorrectly() {
        // given
        Movie mockedMovie1 = mock(Movie.class, CALLS_REAL_METHODS);
        Movie mockedMovie2 = mock(Movie.class, CALLS_REAL_METHODS);
        Rental mockedRental1 = mock(Rental.class, CALLS_REAL_METHODS);
        Rental mockedRental2 = mock(Rental.class, CALLS_REAL_METHODS);
        Customer mockedCustomer = mock(Customer.class, CALLS_REAL_METHODS);
        mockedMovie1.setPriceCode(3);
        mockedMovie1.setTitle("Hardcore Testing");
        mockedMovie1.setResolution("HD");
        mockedMovie2.setPriceCode(3);
        mockedMovie2.setTitle("Softcore Testing");
        mockedMovie2.setResolution("HD");
        mockedRental1.setMovie(mockedMovie1);
        mockedRental1.setDaysRented(1);
        mockedRental1.setDiscount(0.9);
        mockedRental2.setMovie(mockedMovie2);
        mockedRental2.setDaysRented(1);
        mockedRental2.setDiscount(0.9);
        List<Rental> testRentals = new LinkedList<Rental>();
        testRentals.add(mockedRental1);
        testRentals.add(mockedRental2);
        mockedCustomer.setName("Werner");
        mockedCustomer.setRentals(testRentals);

        // when
        mockedCustomer.htmlStatement();

        // then
        then(mockedRental1).should(times(1)).getFrequentRenterPoints();
        then(mockedRental2).should(times(1)).getFrequentRenterPoints();
    }

    // CUSTOMER TESTS END

}